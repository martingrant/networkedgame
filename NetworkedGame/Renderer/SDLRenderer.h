#pragma once

#include <string>
#include <memory>

#ifdef _WIN32
#include <SDL.h>
#include <SDL_ttf.h>
#elif __APPLE__
    #include <TargetConditionals.h>
    #if TARGET_IPHONE_SIMULATOR
        #include "SDL.h"
        #include "SDL_ttf.h"
    #elif TARGET_OS_MAC
        #include <SDL2/SDL.h>
        #include <SDL2_ttf/SDL_ttf.h>
    #endif
#endif

class SDLRenderer
{
	/* Constructors & Destructors */
public:
	SDLRenderer(SDL_Window* window);
	~SDLRenderer();

public:
	void renderTexture(SDL_Texture* sourceTexture, const SDL_Rect* sourceRect, SDL_Rect* destinationRect);
	void renderTexture(std::string textureName, const SDL_Rect* sourceRect, SDL_Rect* destinationRect);
	void renderRect(const SDL_Rect* rect, Uint8 red, Uint8 green, Uint8 blue, Uint8 alpha);
	void setTextureBlendColour(std::string textureName, Uint8 red, Uint8 green, Uint8 blue);
	void renderText(std::string font, std::string text, unsigned int size, int x, int y);

	SDL_Renderer* getRenderer();

private:
	SDL_Renderer* m_renderer;
};

