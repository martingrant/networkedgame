#include "SDLRenderer.h"


SDLRenderer::SDLRenderer(SDL_Window* window)
{
	m_renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	SDL_SetRenderDrawColor(m_renderer, 100, 10, 50, SDL_ALPHA_OPAQUE);
}


SDLRenderer::~SDLRenderer()
{
	SDL_DestroyRenderer(m_renderer);
}


void SDLRenderer::renderTexture(SDL_Texture* sourceTexture, const SDL_Rect* sourceRect, SDL_Rect* destinationRect)
{
	SDL_RenderCopy(m_renderer, sourceTexture, sourceRect, destinationRect);
}


void SDLRenderer::renderTexture(std::string textureName, const SDL_Rect* sourceRect, SDL_Rect* destinationRect)
{
	//SDL_RenderCopy(m_renderer, m_servicesManager->getResourceManager()->getTexture(textureName), sourceRect, destinationRect);
}


void SDLRenderer::renderRect(const SDL_Rect* rect, Uint8 red, Uint8 green, Uint8 blue, Uint8 alpha)
{
	SDL_SetRenderDrawColor(m_renderer, red, green, blue, alpha);
	SDL_RenderFillRect(m_renderer, rect);
	SDL_RenderDrawRect(m_renderer, rect);
	SDL_SetRenderDrawColor(m_renderer, 100, 10, 50, SDL_ALPHA_OPAQUE);
}


void SDLRenderer::setTextureBlendColour(std::string textureName, Uint8 red, Uint8 green, Uint8 blue)
{
	// 255, 255, 255 for normal texture colour
	//SDL_SetTextureColorMod(m_servicesManager->getResourceManager()->getTexture(textureName), red, green, blue);
}


void SDLRenderer::renderText(std::string fontPath, std::string text, unsigned int size, int x, int y)
{
	TTF_Font *font = TTF_OpenFont(fontPath.c_str(), size);
	SDL_Colour test = { 240, 240, 240 };
	SDL_Surface *surf = TTF_RenderText_Blended(font, text.c_str(), test);

	SDL_Rect rect; 
	rect.x = x;
	rect.y = y;
	rect.w = surf->w;
	rect.h = surf->h;

	SDL_Texture *texture = SDL_CreateTextureFromSurface(m_renderer, surf);
	
	SDL_FreeSurface(surf);
	TTF_CloseFont(font);

	renderTexture(texture, NULL, &rect);
}


SDL_Renderer* SDLRenderer::getRenderer()
{ 
	return m_renderer; 
}