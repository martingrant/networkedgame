#pragma once

#ifdef _WIN32
#include <SDL.h>
#elif __APPLE__
#include <SDL2/SDL.h>
#endif

class Player
{
public:
	Player() {}
	Player(int x, int y, int w, int h);
	~Player();

	SDL_Rect& getRect() { return m_rect; }

	void moveUp(int val) { m_rect.y += val; }
	void moveRight(int val) { m_rect.x += val; }
	void setPos(int x, int y) { m_rect.x = x; m_rect.y = y; }

private:
	SDL_Rect m_rect;
};

