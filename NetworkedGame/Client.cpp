#include "Client.h"

Client::Client(std::string host, unsigned int port, unsigned int bufferSize)
{
	m_shutDownClient = false;

	m_serverHost = host;
	m_serverPort = port;
	m_bufferSize = bufferSize;
	m_timeOut = 5000;

	m_buffer = new char[m_bufferSize];

	m_inputLength = 0;

	m_messageCodes[0] = "Connection accepted. (Code 0)";
	m_messageCodes[1] = "Connection denied. Server full. (Code 1)";

	if (SDLNet_Init() == -1)
	{
		std::cout << "SDLNet_Init: " << SDLNet_GetError() << std::endl;
		//exit(EXIT_FAILURE);
	}

	// Allocate socket set
	if (!(m_socketSet = SDLNet_AllocSocketSet(2)))
	{
		std::cout << "SDLNet_AllocSocketSet: " << SDLNet_GetError() << "\n";
		//exit(EXIT_FAILURE);
	}

	// Get server details and client's username
	//std::cout << "Enter server name: ";
	//getline(std::cin, m_serverHost); 
	//serverName = "localhost";
	//std::cout << "Enter username: ";
	//getline(std::cin, m_userName);

	m_connected = false;

	player2X = 150;
	player2Y = 10;
}

Client::~Client(void)
{
	SDLNet_TCP_Close(m_serverSocket);
	SDLNet_TCP_Close(m_clientSocket);

	SDLNet_FreeSocketSet(m_socketSet);
	SDLNet_Quit();

	delete m_buffer;
}


bool Client::connectToServer()
{
	bool connected = false;

	if (!resolveHost()) connected = false;

	// Connect to server
	if (!(m_clientSocket = SDLNet_TCP_Open(&m_serverIP)))
	{
		std::cout << "Failed to open socket to server: " << SDLNet_GetError() << "\n";
		//exit(EXIT_FAILURE);
	}
	else // Check for server response
	{
		std::cout << "Connection opened. Checking for server response." << std::endl << std::endl;

		// Add socket to the socket set for polling
		if(!(SDLNet_TCP_AddSocket(m_socketSet, m_clientSocket)))
		{
			printf("SDLNet_TCP_AddSocket: %s\n", SDLNet_GetError());
		}

		// Check if there is activity from the server socket
		if (SDLNet_CheckSockets(m_socketSet, m_timeOut) < 0)
		{
			printf("SDLNet_CheckSockets: %s\n", SDLNet_GetError());
		}

		// Check if server is sending data
		if (SDLNet_SocketReady(m_clientSocket) != 0)
		{
			std::cout << ">Server: " << m_buffer << " (" << SDLNet_TCP_Recv(m_clientSocket, m_buffer, m_bufferSize) << " bytes)" << std::endl << std::endl;


				connected = true;
				m_connected = true;

		}
		else
		{
			std::cout << "No response received from server." << std::endl << std::endl;
		}
	} 

	return connected;
}

bool Client::update()
{
	//do {
		receiveActivity();
		//sendActivity();
	//} while (!m_shutDownClient);

	return m_shutDownClient;
}

void Client::receiveActivity()
{
	// Check for activity on socket set
	if (SDLNet_CheckSockets(m_socketSet, 0) != 0)
	{
		// Check for server response
		if (SDLNet_SocketReady(m_clientSocket) != 0)
		{
			m_connected = true;
			std::cout << ">Server: " << m_buffer << " (" << SDLNet_TCP_Recv(m_clientSocket, m_buffer, m_bufferSize) << " bytes)" << std::endl;

			if (m_buffer[0] == 'M')
			{
				std::string initialPos = m_buffer;
				std::string pos = initialPos.substr(2, initialPos.size());
				size_t position = pos.find(",");
				std::string posX = pos.substr(0, position);
				std::string posY = pos.substr(position + 1, pos.size());

				player2X = atoi(posX.c_str());
				player2Y = atoi(posY.c_str());
			}


			if (strcmp(m_buffer, "shutdown") == 0)
			{
				std::cout << "Server is shutting down. Disconnecting." << std::endl;
				m_shutDownClient = true;
			}
		}
	} 
}


void Client::sendPlayerPos(int posX, int posY)
{
	std::string string = "M,";
	string += std::to_string(posX);
	string += ",";
	string += std::to_string(posY);

	strcpy(m_buffer, string.c_str());

	//strcpy(m_buffer, std::to_string(pos).c_str());
	m_inputLength = strlen(m_buffer) + 1;

	if (SDLNet_TCP_Send(m_clientSocket, (void *)m_buffer, m_inputLength) < m_inputLength)
	{
		std::cout << "SDLNet_TCP_Send: " << SDLNet_GetError() << std::endl;
	}
}

void Client::sendUserMessages(std::string text)
{
    std::string newText = "T,";
    newText += text;
    strcpy(m_buffer, newText.c_str());
    
    m_inputLength = strlen(m_buffer) + 1;
    
    if (SDLNet_TCP_Send(m_clientSocket, (void *)m_buffer, m_inputLength) < m_inputLength)
    {
        std::cout << "SDLNet_TCP_Send: " << SDLNet_GetError() << std::endl;
    }
}

bool Client::messageAwaiting()
{
    if (m_buffer[0] == 'T')
        return true;
    else return false;
}

std::string Client::getMessages()
{
    return std::string(m_buffer);
}


void Client::sendActivity()
{
//	bool getInput = false;
//	bool sendMessage = false; 
//
//	while (m_shutDownClient == false)
//	{
//		if (getInput == false)
//		{
//			std::cout << std::endl << "Enter message:" << std::endl;
//			getInput = true;
//		}
//
//		int status = _kbhit();
//
//		if (status != 0)
//		{ 
//			char keyPress = _getch();
//
//			// Print the keypress
//			std::cout << keyPress;
//			if ((int)keyPress == 8) 
//			{
//				std::cout << " " << "\b" ;
//				m_userInput.pop_back();
//			} 
//			else 
//			{
//				// Flush the character to the screen, clear IO buffer, send to OS
//				fflush(stdout);
//
//				// Add input to string if character is not enter key
//				if ((int)keyPress != 13)
//				{                
//					m_userInput += keyPress;
//				}
//				else // Send message when enter is pressed
//				{
//					strcpy(m_buffer, m_userInput.c_str());
//					m_inputLength = strlen(m_buffer) + 1;
//
//					if (SDLNet_TCP_Send(m_clientSocket, (void *)m_buffer, m_inputLength) < m_inputLength)
//					{
//						std::cout << "SDLNet_TCP_Send: " << SDLNet_GetError() << std::endl;
//					}
//					else
//					{   
//						// Input action flags
//						if (sendMessage == true && (m_userInput == "quit" || m_userInput == "exit" || m_userInput == "shutdown"))
//						{
//							m_shutDownClient = true;
//						}
//
//						// Reset for the next message
//						std::cout << std::endl;
//						getInput = false;
//						sendMessage = false;
//						m_userInput = "";
//					}
//				}     
//			} 
//		}
//	}
}

bool Client::resolveHost()
{
	bool resolved = false;

	// Resolve host to IP
	if (SDLNet_ResolveHost(&m_serverIP, m_serverHost.c_str(), m_serverPort) < 0)
	{
		std::cout << "SDLNet_ResolveHost: " << SDLNet_GetError();
		resolved = false;
	}
	else
	{
		std::cout << "Resolved host to IP: " << convertIP(m_serverIP) << std::endl;
		resolved = true;
	}

	const char* hostName;
	// Resolve server IP to host name
	if ((hostName = SDLNet_ResolveIP(&m_serverIP)) == NULL)
	{
		std::cout << "SDLNet_ResolveIP: " << SDLNet_GetError() << std::endl;
		resolved = false;
	}
	else
	{
		m_serverHost = hostName;
		std::cout << "Resolved IP to host: " << m_serverHost << std::endl << std::endl;
		resolved = true;
	}

	return resolved;
}

std::string Client::convertIP(IPaddress address)
{
	std::stringstream ss;
	Uint8 * dotQuad;

	dotQuad = (Uint8*)&address.host;

	ss << (unsigned short)dotQuad[0] << "." << (unsigned short)dotQuad[1] << '.' << (unsigned short)dotQuad[2] << '.' << (unsigned short)dotQuad[3] << ':' << SDLNet_Read16(&address.port);

	return ss.str();
}

std::string Client::convertIP(TCPsocket socket)
{
	std::stringstream ss;
	Uint8 * dotQuad;

	IPaddress tempIP;

	tempIP.operator=(*SDLNet_TCP_GetPeerAddress(socket));

	// Get IP in dot-quad format, break up 32-bit unsigned host address, split it into array of four 8-but unsigned numbers
	dotQuad = (Uint8*)&tempIP.host;

	// Cast to ints, read last 16 bits for port number
	ss << (unsigned short)dotQuad[0] << "." << (unsigned short)dotQuad[1] << '.' << (unsigned short)dotQuad[2] << '.' << (unsigned short)dotQuad[3] << ':' << SDLNet_Read16(&tempIP.port);

	return ss.str();
}