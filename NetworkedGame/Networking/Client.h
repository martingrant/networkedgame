#pragma once

#include <iostream>
#include <string>
#include <sstream>
//#include <conio.h>	// non-blocking input, _khbit, _getche etc.
#include <array>

#ifdef _WIN32
#include <SDL_net.h>
#elif __APPLE__
#include <SDL2_net/SDL_net.h>
#endif

class Client
{
public:
	/* Class Constructor & Destructor */
	Client(std::string host, unsigned int port, unsigned int bufferSize);
	~Client(void);

public:
	/* General Public Methods */
	void update();

public:
	/* Client Management */
	bool connectToServer();
	void disconnectFromServer();
	bool getUserInput();
	bool getStatus();

public:
	/* Data Transmission Methods */
	void receiveData();
	void sendData();
	
private:
	/* Socket Methods */
	void initSocketSet();
	bool resolveHost();

private:
	/* General Client Member Variables */
	bool m_connected;
	bool m_getInput;

	std::string m_userName;
	std::string m_serverHost;
	unsigned int m_serverPort;
	unsigned int m_bufferSize;
	unsigned int m_timeOut;

	IPaddress m_serverIP;
	TCPsocket m_clientSocket;
	SDLNet_SocketSet m_socketSet;

	char* m_buffer;

	std::string m_userInput;
	int m_inputLength;

	std::array<std::string, 10> m_messageCodes;
	std::array<std::string, 10> m_userCommands;

private:
	/* Buffer Methods */
	const char* getBufferData();
	void printBuffer();
	void copyToBuffer(const char* data);
	void clearBuffer();

	/* User Commands */
	void parseForUserCommands(std::string userInput);
	void printInfo();
	void printHelp();

	/* IP Methods */
	std::string convertIP(IPaddress address);
	std::string convertIP(TCPsocket socket);
	
public:
	/* Game Management */
	bool gameInSession();
	int getPlayerNumber();

private:
	/* Game Management */
	void startSession();
	void pauseSession();
	void resumeSession();
	void endSession();
	void runGameSession();

	void setReady(bool toggle);
	bool isReady();

	void setPlayerNumber(int number);
	

private:
	/* Game Management */
	bool m_ready;
	bool m_gameInSession;
	bool m_gamePaused;

	int m_playerNumber;
};

