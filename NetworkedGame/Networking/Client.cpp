#include "Client.h"


#pragma region Class Constructor & Destructor

Client::Client(std::string host, unsigned int port, unsigned int bufferSize)
{
	std::cout << "Client constructor called" << std::endl;

	m_connected = false;
	m_getInput = true;

	// Set username to name of machine
	m_userName = getenv("COMPUTERNAME");
	m_serverHost = host;
	m_serverPort = port;
	m_bufferSize = bufferSize;	// maximum buffer size for data
	m_timeOut = 5000;			// time out for polling socket, measured in milliseconds

	m_buffer;

	m_inputLength = 0;

	m_messageCodes[0] = "Connection accepted. (Code 0)";
	m_messageCodes[1] = "Connection denied. Server full. (Code 1)";
	m_messageCodes[2] = "Game session ready. (Code 2)";

	m_userCommands[0] = "/shutdown";
	m_userCommands[1] = "/disconnect";
	m_userCommands[2] = "/connect";
	m_userCommands[3] = "/info";
	m_userCommands[4] = "/help";
	m_userCommands[5] = "/ready";
	m_userCommands[6] = "/pause";
	m_userCommands[7] = "/resume";
	m_userCommands[8] = "/end";

	m_ready = false;
	m_gameInSession = false;
	m_gamePaused = false;
	m_playerNumber = -1;

	initSocketSet();
}


Client::~Client(void)
{
	std::cout << "Client constructor called" << std::endl;

	SDLNet_TCP_Close(m_clientSocket);

	SDLNet_FreeSocketSet(m_socketSet);
}

#pragma endregion


#pragma region Public Methods (Init, Update)

void Client::update()
{
	if (m_connected)
	{
		receiveData();

		// If server is assigning this client to be player 0
		if (strcmp("player0", getBufferData()) == 0)
		{
			setPlayerNumber(0);
			startSession();
		}

		// If server is assinging this client to be player 1
		if (strcmp("player1", getBufferData()) == 0)
		{
			setPlayerNumber(1);
			startSession();
		}

		if (m_gameInSession == true)
		{
			runGameSession();
		} 
	}

	// Send any data input by client to console
	if (getUserInput() == true)
	{	
		sendData();
	}
}

#pragma endregion


#pragma region Client Management

bool Client::connectToServer()
{
	bool connected = false;

	// Resolve host
	if (!resolveHost()) connected = false;

	// Connect to server
	if (!(m_clientSocket = SDLNet_TCP_Open(&m_serverIP)))
	{
		std::cout << "Failed to open socket to server: " << SDLNet_GetError() << "\n";
	}
	else // Check for server response
	{
		std::cout << "Connection opened. Checking for server response." << std::endl << std::endl;

		// Add socket to the socket set for polling
		if(!(SDLNet_TCP_AddSocket(m_socketSet, m_clientSocket)))
		{
			printf("SDLNet_TCP_AddSocket: %s\n", SDLNet_GetError());
		}

		// Check if there is activity from the server socket
		if (SDLNet_CheckSockets(m_socketSet, m_timeOut) < 0)
		{
			printf("SDLNet_CheckSockets: %s\n", SDLNet_GetError());
		}

		// Check if server is sending data
		if (SDLNet_SocketReady(m_clientSocket) != 0)
		{
			receiveData();

			// Join server if server accepted
			if (strcmp(m_buffer, m_messageCodes[0].c_str()) == 0)
			{
				// Set connection flag
				m_connected = true;
				connected = true;

				std::cout << "Joining server as '" << m_userName << "' from: " << convertIP(m_clientSocket) << std::endl << std::endl;

				// Send username to server
				copyToBuffer(m_userName.c_str());
				sendData();
			}
			else
			{
				std::cout << "Server is full. Closing connection." << std::endl << std::endl;
			}
		}
		else
		{
			std::cout << "No response received from server." << std::endl << std::endl;
		}
	} 

	return connected;
}


void Client::disconnectFromServer()
{
	m_connected = false;

	SDLNet_TCP_Close(m_clientSocket);

	std::cout << "Disconnected from server." << std::endl;
}


bool Client::getUserInput()
{
//	bool sendMessage = false; 
//
//	if (m_getInput == true)
//	{
//		std::cout << std::endl << "Enter message:" << std::endl;
//	}
//
//	if (_kbhit() != 0)
//	{ 
//		char keyPress = _getch();
//
//		// Print the keypress
//		std::cout << keyPress;
//
//		// Check if backspace has been pressed
//		if ((int)keyPress == 8) 
//		{
//			// Delete the last character only if there are characters present (can't backspace if empty)
//			if (m_userInput.size() != 0) 
//			{
//				std::cout << " " << "\b";
//				m_userInput.pop_back();
//			}
//		} 
//		else 
//		{
//			// Flush the character to the screen, clear IO buffer, send to OS
//			fflush(stdout);
//
//			// Add input to string if character is not enter key
//			if ((int)keyPress != 13)
//			{                
//				m_userInput += keyPress;
//				m_getInput = false;
//			}
//			else // Copy user input to buffer when enter has been pressed
//			{
//				m_getInput = true;
//
//				copyToBuffer(m_userInput.c_str());
//
//				parseForUserCommands(m_userInput);
//
//				// Reset for the next message
//				std::cout << std::endl;
//				sendMessage = true;
//				m_userInput = "";
//			}     
//		} 
//	} else m_getInput = false;
//
//	return sendMessage;
    return true;
}


bool Client::getStatus()
{
	return m_connected;
}

#pragma endregion


#pragma region Data Transmission Methods

void Client::receiveData()
{
	// Check for activity on socket set
	if (SDLNet_CheckSockets(m_socketSet, 0) != 0)
	{
		// Check for server response
		if (SDLNet_SocketReady(m_clientSocket) != 0)
		{
			SDLNet_TCP_Recv(m_clientSocket, m_buffer, m_bufferSize);
			//if (SDLNet_TCP_Recv(m_clientSocket, m_buffer.get(), m_bufferSize) < 1)
			//{
				if (strcmp(m_buffer, m_userCommands[0].c_str()) == 0)
				{
					std::cout << "Server is shutting down. Disconnecting." << std::endl;
					m_connected = false;
				}
				else
				{
					printBuffer();
				}
			//}	
		}
	} //else m_connected = false;
}


void Client::sendData()
{
	if (SDLNet_TCP_Send(m_clientSocket, (void *)m_buffer, m_inputLength) < m_inputLength)
	{
		std::cout << "SDLNet_TCP_Send: " << SDLNet_GetError() << std::endl;
	}
}

#pragma endregion


#pragma region Socket Methods

void Client::initSocketSet()
{
	// Allocate socket set
	if (!(m_socketSet = SDLNet_AllocSocketSet(1)))
	{
		std::cout << "SDLNet_AllocSocketSet: " << SDLNet_GetError() << "\n";
	}
}


bool Client::resolveHost()
{
	bool resolved = false;

	// Resolve host to IP
	if (SDLNet_ResolveHost(&m_serverIP, m_serverHost.c_str(), m_serverPort) < 0)
	{
		std::cout << "SDLNet_ResolveHost: " << SDLNet_GetError();
		resolved = false;
	}
	else
	{
		std::cout << "Resolved host to IP: " << convertIP(m_serverIP) << std::endl;
		resolved = true;
	}

	const char* hostName;
	// Resolve server IP to host name
	if ((hostName = SDLNet_ResolveIP(&m_serverIP)) == NULL)
	{
		std::cout << "SDLNet_ResolveIP: " << SDLNet_GetError() << std::endl;
		resolved = false;
	}
	else
	{
		m_serverHost = hostName;
		std::cout << "Resolved IP to host: " << m_serverHost << std::endl << std::endl;
		resolved = true;
	}

	return resolved;
}

#pragma endregion


#pragma region Buffer Methods

const char* Client::getBufferData()
{
	return m_buffer;
}


void Client::printBuffer()
{
	std::cout << ">Server: " << getBufferData() << std::endl;
}


void Client::copyToBuffer(const char* data)
{
	strcpy(m_buffer, data);
	m_inputLength = strlen(m_buffer) + 1;	// + 1 for teminating character
}


void Client::clearBuffer()
{
	const char* clear = "";
	strcpy(m_buffer, clear);
}

#pragma endregion


#pragma region User Commands

void Client::parseForUserCommands(std::string userInput)
{
	if (userInput == m_userCommands[0]) m_connected = false; 
	if (userInput == m_userCommands[1]) disconnectFromServer();
	if (userInput == m_userCommands[2]) connectToServer();
	if (userInput == m_userCommands[3]) printInfo();
	if (userInput == m_userCommands[4]) printHelp();
	if (userInput == m_userCommands[5]) setReady(true); 
}


void Client::printInfo()
{
	// Print connection info

	std::cout << "Network information: " << std::endl;

	std::cout << "Client: " << m_userName << std::endl;

	std::cout << "Server: " << m_serverHost << " " << convertIP(m_serverIP) << " " << convertIP(m_clientSocket) << std::endl;
}


void Client::printHelp()
{
	// Print available user commands

	std::cout << std::endl << std::endl << "// Console Help //" << std::endl << "Available commands:" << std::endl << std::endl;
	for (auto iterator = m_userCommands.begin(); iterator != m_userCommands.end(); ++iterator)
	{
		std::cout << *iterator << std::endl;
	}
}

#pragma endregion


#pragma region IP Methods

std::string Client::convertIP(IPaddress address)
{
	// Used for printing IP address using IPaddress data type

	std::stringstream ss;
	Uint8 * dotQuad;

	dotQuad = (Uint8*)&address.host;

	ss << (unsigned short)dotQuad[0] << "." << (unsigned short)dotQuad[1] << '.' << (unsigned short)dotQuad[2] << '.' << (unsigned short)dotQuad[3] << ':' << SDLNet_Read16(&address.port);

	return ss.str();
}


std::string Client::convertIP(TCPsocket socket)
{
	// Used for printing IP address using TCPsocket data type

	std::stringstream ss;
	Uint8 * dotQuad;

	IPaddress tempIP;

	tempIP.operator=(*SDLNet_TCP_GetPeerAddress(socket));

	// Get IP in dot-quad format, break up 32-bit unsigned host address, split it into array of four 8-but unsigned numbers
	dotQuad = (Uint8*)&tempIP.host;

	// Cast to ints, read last 16 bits for port number
	ss << (unsigned short)dotQuad[0] << "." << (unsigned short)dotQuad[1] << '.' << (unsigned short)dotQuad[2] << '.' << (unsigned short)dotQuad[3] << ':' << SDLNet_Read16(&tempIP.port);

	return ss.str();
}

#pragma endregion


#pragma region Game Management

void Client::startSession()
{ 
	m_gameInSession = true;
}


void Client::pauseSession()
{
	m_gamePaused = true;
}


void Client::resumeSession()
{
	m_gamePaused = false;
}


void Client::endSession()
{
	m_gameInSession = false;
}


void Client::runGameSession()
{
	//std::cout << "running game session!" << std::endl;
}


bool Client::gameInSession()
{
	return m_gameInSession;
}


void Client::setReady(bool toggle)
{
	m_ready = toggle;
}


bool Client::isReady()
{
	return m_ready;
}


void Client::setPlayerNumber(int number)
{
	m_playerNumber = number;
	//std::cout << "player number: " << m_playerNumber << std::endl;
}


int Client::getPlayerNumber()
{
	return m_playerNumber;
}

#pragma endregion