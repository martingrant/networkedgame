#pragma once

#include <iostream>
#include <string>
#include <sstream>
//#include <conio.h>	// non-blocking input, _khbit, _getche etc.

#ifdef _WIN32
#include <SDL_net.h>
#elif __APPLE__
#include <SDL2_net/SDL_net.h>
#endif

class Client
{
public:
	Client(std::string host, unsigned int port, unsigned int bufferSize);
	~Client(void);

	bool connectToServer();
	bool update();
	void receiveActivity();
	void sendActivity();
    void sendUserMessages(std::string text);
    bool messageAwaiting();
    std::string getMessages();

	void sendPlayerPos(int posX, int posY);
	int getPlayer2X()
	{ 
		return player2X;
	}
	int getPlayer2Y() { return player2Y; }


	bool checkIfConnected() { return m_connected; }

private:
	bool m_shutDownClient;
	
	bool m_connected;

	unsigned int m_serverPort;
	unsigned int m_bufferSize;
	unsigned int m_timeOut;

	std::string m_userName;

	std::string m_serverHost;
	IPaddress m_serverIP;
	TCPsocket m_serverSocket;
	TCPsocket m_clientSocket;
	SDLNet_SocketSet m_socketSet;

	char* m_buffer;
	std::string m_userInput;
	int m_inputLength;

	const char* m_messageCodes[10];

	int player2X;
	int player2Y;

private:
	bool resolveHost();

	std::string convertIP(IPaddress address);
	std::string convertIP(TCPsocket socket);
};

