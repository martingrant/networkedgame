#include <iostream>
#include <vector>

#ifdef _WIN32
#include <SDL.h>
#include <SDL_net.h>
#include <SDL_ttf.h>
#elif __APPLE__
#include <SDL2/SDL.h>
#include <SDL2_net/SDL_net.h>
#include <SDL2_ttf/SDL_ttf.h>
#endif

#include "Input/SDLInput.h"
#include "Player.h"
#include "Client.h"
#include "Screens/SDLScreenManager.h"
#include "Screens/Screen.h"

SDL_Window* window;
SDL_Renderer* renderer;
SDL_Event events;
bool running = true;

SDLScreenManager* screenManager;

SDLInput input = SDLInput();

Client* network;





//void renderMessageLog()
//{
//    if (messageLog.size() > 0)
//    {
//        for (unsigned int index = 0; index < messageLog.size(); ++index)
//        {
//            //renderText(messageLog[index], 10, messageLogY);
//            messageLogY += 20;
//        }
//        
//    }
//}
//
//void clearMessageLog()
//{
//    messageLog.clear();
//    messageLogY = 300;
//}



struct gameScreen
{
	//void update()
	//{
	//	if (messageLog.size() > 5) clearMessageLog();

	//	if (network->messageAwaiting())
	//	{
	//		messageLog.push_back(network->getMessages());
	//	}

	//	if (input.getKeyState("W"))
	//	{
	//		player1.moveUp(-1);
	//		network->sendPlayerPos(player1.getRect().x, player1.getRect().y);
	//	}

	//	if (input.getKeyState("S"))
	//	{
	//		player1.moveUp(1);
	//		network->sendPlayerPos(player1.getRect().x, player1.getRect().y);
	//	}

	//	if (input.getKeyState("A"))
	//	{
	//		player1.moveRight(-1);
	//		network->sendPlayerPos(player1.getRect().x, player1.getRect().y);
	//	}

	//	if (input.getKeyState("D"))
	//	{
	//		player1.moveRight(1);
	//		network->sendPlayerPos(player1.getRect().x, player1.getRect().y);
	//	}

	//	player2.setPos(network->getPlayer2X(), network->getPlayer2Y());
	//}
	//void render()
	//{
	//	renderPlayer(player1, 0, 10, 50, 100);
	//	renderPlayer(player2, 0, 200, 150, 100);

	//	renderText(userPrompt, 10, 400);
	//	renderText(userInput, 100, 400);

	//	renderMessageLog();
	//}
};


void update(SDL_Event& event)
{
    network->update();
}


void render()
{
	SDL_SetRenderDrawColor(renderer, 100, 10, 50, SDL_ALPHA_OPAQUE);
	SDL_RenderClear(renderer);
	
	screenManager->render();

	SDL_RenderPresent(renderer);
}


int main(int argc, char *args[])
{
	network = new Client("localhost", 1234, 512);

	screenManager = new SDLScreenManager("NetworkGame", 640, 480, network);

	TTF_Init();

	
	network->connectToServer();

	// need return from internal loop
	while (running == true)
	{
		screenManager->update();
		screenManager->render();
	}

	return 0;
}

				