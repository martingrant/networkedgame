#pragma once

#include <memory>

#include "../Screens/SDLScreenManager.h"
#include "../ServicesManager/ServicesManager.h"
#include "../Time/TimeManager.h"
#include "../Resources/ResourceManager.h"

class Application
{
public:
	/* Class Constructors & Destructors*/
	Application();
	~Application();

public:
	/* General Public Methods */
	void init();
	bool run();

private:
	bool m_running;
	SDL_Event m_SDLEvent;

	std::shared_ptr<SDLScreenManager> m_screenManager;
	std::shared_ptr<ServicesManager> m_servicesManager;
	std::shared_ptr<TimeManager> m_timeManager;
};