#include "Application.h"

#include <time.h>

#pragma region Class Constructors & Destructors
/*
* Constructs the Application class. Sets m_running to true and defines ScreenManager.
*/
Application::Application()
{
	std::cout << "Constructing Application..." << std::endl << std::endl;

	m_servicesManager = std::shared_ptr<ServicesManager>(new ServicesManager());

	m_timeManager = m_servicesManager->getTimeManager();

	m_screenManager = std::shared_ptr<SDLScreenManager>(new SDLScreenManager("How To Rule The World In 3 Minutes (SDL)", 1280, 720, m_servicesManager));

	m_servicesManager->registerService(m_screenManager);

	m_running = true;
}

/*
* Destructs the Application class.
*/
Application::~Application()
{
	std::cout << "Destructed Application." << std::endl << std::endl;
}

#pragma endregion


#pragma region General Public Methods
/*
* Application loop. Loop will run whilst m_running is set to true, and will stop once m_running has been set to false.
* Checks for SDL events and updates m_screenManager.
*
* @return m_running - Returns a boolean variable which signifys if the application loop is currently running or not.
*/
bool Application::run()
{
	while (m_running == true)
	{
		while (SDL_PollEvent(&m_SDLEvent))
		{
			switch (m_SDLEvent.type)
			{
			case SDL_WINDOWEVENT:
				switch (m_SDLEvent.window.event)
				{
				case SDL_WINDOWEVENT_CLOSE:
					m_running = false;
					break;
				}
				break;
			case SDL_QUIT:
				m_running = false;
				break;
			}

			if (m_servicesManager->getInputManager()->getKeyState("ESCAPE") == true) m_running = false;
		}

		m_servicesManager->getInputManager()->update();

		m_timeManager->resetNumberOfLoops();

		while (m_timeManager->getTickCount() > m_timeManager->getNextGameTick() && m_timeManager->getNumberOfLoops() < m_timeManager->getMaxFrameSkip())
		{
			m_screenManager->update();

			m_timeManager->incrementNextGameTick(); 
			m_timeManager->incrementNumberOfLoops(); 
		}

		m_timeManager->setInterpolation();

		m_screenManager->render(); 

	}
	return m_running;
}

#pragma endregion