#include "MainMenuScreen.h"


#pragma region Constructors & Destructors

MainMenuScreen::MainMenuScreen(std::shared_ptr<SDLRenderer> renderer, ScreenManager* screenManager, Client* client)
{
	m_renderer = renderer;
	m_screenManager = screenManager; 
	m_client = client;

	m_textInput = true;
	m_userInput = " ";
	m_userNamePrompt = "Enter Username:";
	m_serverPrompt = "Enter Server IP:";
	m_enterCount = 0;
}


MainMenuScreen::~MainMenuScreen()
{
}

#pragma endregion

#pragma region General Public Methods

void MainMenuScreen::update(SDL_Event& events)
{
	switch (events.type)
	{
	case SDL_KEYDOWN:
		if (events.key.keysym.scancode == SDL_SCANCODE_RETURN)
		{
			if (m_textInput == false)
			{
				//m_textInput = true;
			}
			else
			{
				//m_textInput = false;
				m_client->sendUserMessages(m_userInput);
				m_userInput = " ";
			}

			++m_enterCount;
		}
		if (m_textInput == true)
		{
			if (events.key.keysym.scancode == SDL_SCANCODE_BACKSPACE)
			{
				if (m_userInput.size() > 2)
				{
					std::cout << " " << "\b";
					m_userInput.pop_back();
				}
				else if (m_userInput.size() < 2)
				{
					m_userInput += " ";
				}
			}
		}

		break;
	case SDL_TEXTINPUT:
		//inputText += SDL_GetKeyName(SDL_GetKeyFromScancode(e.key.keysym.scancode));
		if (m_textInput == true)
			m_userInput += events.text.text;
		break;
	}
	

	if (m_enterCount == 2)
	{
		m_screenManager->setCurrentScreen("GameScreen");
	}
}


void MainMenuScreen::render()
{
	if (m_enterCount == 0)
	{
		m_renderer->renderText("MONOFONT.TTF", m_userNamePrompt, 16, 200, 300);
	}
	else
	{
		m_renderer->renderText("MONOFONT.TTF", m_serverPrompt, 16, 200, 300);
	}

	m_renderer->renderText("MONOFONT.TTF", m_userInput, 16, 300, 300);
}


void MainMenuScreen::onActivated()
{
}


void MainMenuScreen::onDeactivated()
{
}

#pragma endregion
