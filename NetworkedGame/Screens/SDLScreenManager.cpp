#include "SDLScreenManager.h"


#pragma region Class Constructors & Destructors
/*
 * Constructs SDLScreenManager class.
 */
SDLScreenManager::SDLScreenManager(std::string title, unsigned int width, unsigned int height, Client* client)
{
	// Init SDl
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
	{
		std::cout << "SDL_Init: " << SDL_GetError() << std::endl;
	}
    
	// Set up class members
	m_displayDevice = 0;
	m_windowTitle = title;
	m_windowWidth = width;
	m_windowHeight = height;

	m_running = true;

	createWindow();

	m_renderer = std::shared_ptr<SDLRenderer>(new SDLRenderer(m_window));

	// Setup screens
	m_screenMap["MainMenuScreen"] = std::shared_ptr<Screen>(new MainMenuScreen(m_renderer, this, client));
	m_screenMap["GameScreen"] = std::shared_ptr<Screen>(new GameScreen(m_renderer, this, client));

	setCurrentScreen("MainMenuScreen");

	std::cout << "SDLScreenManager constructed." << std::endl;
}

/*
 * Destructs SDLScreenManager class.
 */
SDLScreenManager::~SDLScreenManager()
{
	SDL_DestroyWindow(m_window);
	SDL_Quit();

	std::cout << "SDLScreenManager destructed." << std::endl;
}

#pragma endregion


#pragma region General Public Methods

/*
 * Updates the current screen.
 */
void SDLScreenManager::update()
{
	while (SDL_PollEvent(&m_events))
	{
		switch (m_events.type)
		{
		case SDL_WINDOWEVENT:
			switch (m_events.window.event)
			{
			case SDL_WINDOWEVENT_CLOSE:
				m_running = false;
				break;
			}
			break;
		case SDL_QUIT:
			m_running = false;
			break;
		}

		m_screenMap["CurrentScreen"]->update(m_events);
	}

}

/*
 * Renders the current screen.
 */
void SDLScreenManager::render()
{
	SDL_RenderClear(m_renderer->getRenderer());

	m_screenMap["CurrentScreen"]->render();

	SDL_RenderPresent(m_renderer->getRenderer());
}

#pragma endregion


#pragma region Screen Management 

void SDLScreenManager::setCurrentScreen(std::string window)
{
	if (m_screenMap["CurrentScreen"] != nullptr) m_screenMap["CurrentScreen"]->onDeactivated();
	m_screenMap["CurrentScreen"] = m_screenMap[window];
	m_screenMap["CurrentScreen"]->onActivated();
}

#pragma endregion


#pragma region Window Management
/*
 * Creates new SDL window with an OpenGL context attached to it.
 * Sets OpenGL version and profile to be used.
 * Sets which display device to display the window on.
 * Sets VSync to be enabled.
 *
 * @param std::string title - title of the window to be created.
 */
void SDLScreenManager::createWindow()
{
	setUpDisplayModeVector();

	Uint32 windowFlags = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN;

#if defined(_RELEASE) || defined(RELEASE)
	windowFlags = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_FULLSCREEN;
	m_windowWidth = m_displayModeVector[0].w;
	m_windowHeight = m_displayModeVector[0].h;
#endif // #if defined(_RELEASE) || defined(RELEASE)
    
    int GLMajorVersion = 4;
    int GLMinorVersion = 0;

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, GLMajorVersion);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, GLMinorVersion);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    
	// Get number of displays, if more than 1 then create window on display device 1 (second monitor)
	if (SDL_GetNumVideoDisplays() > 1)
	{
		m_displayDevice = 1;
	}
	else
	{
		m_displayDevice = 0;
	}
	
	m_window = SDL_CreateWindow(m_windowTitle.c_str(), SDL_WINDOWPOS_CENTERED_DISPLAY(m_displayDevice), SDL_WINDOWPOS_CENTERED_DISPLAY(m_displayDevice), m_windowWidth, m_windowHeight, windowFlags);

	if (m_window == NULL)
	{
		std::cout << "SDL_CreateWindow: " << SDL_GetError() << std::endl;
	}

	SDL_GL_SetSwapInterval(1);
}

/*
* Set the title of the SDL window.
*
* @param std::string title - title of the window to be created.
*/
void SDLScreenManager::setWindowTitle(std::string title)
{
	m_windowTitle = title;
	SDL_SetWindowTitle(m_window, m_windowTitle.c_str());
}

/*
* Set the size of the SDL window.
*
* @param int width - the value to set for the width of the window.
* @param int height - the value to set for the height of the window.
*/
void SDLScreenManager::setWindowSize(int width, int height)
{
	m_windowWidth = width;
	m_windowHeight = height;
	SDL_SetWindowSize(m_window, m_windowWidth, m_windowHeight);
}

/*
* Set the width of the SDL window.
*
* @param int width - the value to set for the width of the window.
*/
void SDLScreenManager::setWindowWidth(int width)
{
	m_windowWidth = width;
	SDL_SetWindowSize(m_window, m_windowWidth, m_windowHeight);
}

/*
* Set the height of the SDL window.
*
* @param int height - the value to set for the height of the window.
*/
void SDLScreenManager::setWindowHeight(int height)
{
	m_windowHeight = height; 
	SDL_SetWindowSize(m_window, m_windowWidth, m_windowHeight);
}

/*
* Minimise the window.
*/
void SDLScreenManager::minimiseWindow()
{
	SDL_MinimizeWindow(m_window);
}

/*
* Restore the window.
*/
void SDLScreenManager::restoreWindow()
{
	SDL_RestoreWindow(m_window);
}

/*
* Maximise the window.
*/
void SDLScreenManager::maximiseWindow()
{
	SDL_MaximizeWindow(m_window);
}

/*
* Set the window to full screen mode.
*/
void SDLScreenManager::setFullScreenMode()
{
	SDL_SetWindowFullscreen(m_window, SDL_WINDOW_FULLSCREEN);
}

/*
* Set the window to windowed mode.
*/
void SDLScreenManager::setWindowedMode()
{
	SDL_SetWindowFullscreen(m_window, 0);
}

/*
* Set up the DisplayMode vector, storing all display modes for the current display screen.
*/
void SDLScreenManager::setUpDisplayModeVector()
{
	unsigned int numberOfDisplayModes = SDL_GetNumDisplayModes(m_displayDevice);
	SDL_DisplayMode tempDisplayMode;
	std::vector<SDL_DisplayMode> tempVector;
	unsigned int tempHeight;
	unsigned int tempWidth;

	// If function has been called before (vectors have been used) then clear them for next call
	// Used when game window has changed to another screen index
	if (m_displayModeVector.size() > 0 && tempVector.size() > 0)
	{
		m_displayModeVector.clear();
		tempVector.clear();
	}

	// Fill array with all display modes available to current screen
	for (unsigned int index = 0; index < numberOfDisplayModes; ++index)
	{
		SDL_GetDisplayMode(m_displayDevice, index, &tempDisplayMode);

		tempVector.push_back(tempDisplayMode);
	}

	// Check for duplicates by comparing height and width values, adding unique values to the member vector
	for (unsigned int index = 0; index < tempVector.size(); ++index)
	{
		if (index == 0)
		{
			m_displayModeVector.push_back(tempVector[index]);
			tempHeight = tempVector[index].h;
			tempWidth = tempVector[index].w;
		}
		else
		{
			if (tempHeight != tempVector[index].h /*&& tempWidth != tempVector[index].w*/)
			{
				m_displayModeVector.push_back(tempVector[index]);
				tempHeight = tempVector[index].h;
				tempWidth = tempVector[index].w;
			}
		}
	}
}

#pragma endregion