#include "GameScreen.h"


#pragma region Constructors & Destructors

GameScreen::GameScreen(std::shared_ptr<SDLRenderer> renderer, ScreenManager* screenManager, Client* client)
{
	m_renderer = renderer;
	m_screenManager = screenManager;
	m_client = client;

	player1 = Player(10, 10, 100, 100);
	player2 = Player(150, 10, 100, 100);

	m_userInput = "";
	m_textInput = false;
	m_userPrompt = "Enter Message:";

	messageLogY = 300;
}


GameScreen::~GameScreen()
{
}

#pragma endregion

#pragma region General Public Methods

void GameScreen::update(SDL_Event& events)
{
	switch (events.type)
	{
	case SDL_KEYDOWN:
		if (events.key.keysym.scancode == SDL_SCANCODE_RETURN)
		{
			if (m_textInput == false)
			{
				m_textInput = true;
			}
			else
			{
				messageLog.push_back(m_userInput);
				//messageLogY += 10;

				m_textInput = false;
				m_client->sendUserMessages(m_userInput);
				m_userInput = "";
			}
		}
		if (m_textInput == true)
		{
			if (events.key.keysym.scancode == SDL_SCANCODE_BACKSPACE)
			{
				if (m_userInput.size() > 2)
				{
					std::cout << " " << "\b";
					m_userInput.pop_back();
				}
				else if (m_userInput.size() < 2)
				{
					m_userInput += " ";
				}
			}
		}

		break;
	case SDL_TEXTINPUT:
		//inputText += SDL_GetKeyName(SDL_GetKeyFromScancode(e.key.keysym.scancode));
		if (m_textInput == true)
			m_userInput += events.text.text;
		break;
	}

	if (messageLog.size() > 4)
	{
		messageLog.clear();
	}
}


void GameScreen::render()
{
	m_renderer->renderRect(&player1.getRect(), 0, 10, 50, 100);
	m_renderer->renderRect(&player1.getRect(), 0, 200, 150, 100);

	m_renderer->renderText("MONOFONT.TTF", m_userPrompt, 16, 10, 400);
	if (m_userInput.size() > 1)
		m_renderer->renderText("MONOFONT.TTF", m_userInput, 16, 100, 400);

	messageLogY = 300;
	for (unsigned int index = 0; index < messageLog.size(); ++index)
	{
		m_renderer->renderText("MONOFONT.TTF", messageLog[index], 16, 10, messageLogY);
		messageLogY += 20;
	}
}


void GameScreen::onActivated()
{

}


void GameScreen::onDeactivated()
{

}

#pragma endregion
