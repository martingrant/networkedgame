#pragma once

#include <memory>
#include <string>
#include <iostream>

#ifdef _WIN32
#include <SDL.h>
#elif __APPLE__
    #include <TargetConditionals.h>
    #if TARGET_IPHONE_SIMULATOR
        #include "SDL.h"
    #elif TARGET_OS_MAC
        #include <SDL2/SDL.h>
    #endif
#endif

#include "../Renderer/SDLRenderer.h"
#include "Screen.h"
#include "ScreenManager.h"
#include "../Client.h"

class MainMenuScreen : public Screen
{
	/* Constructors & Destructors */
public:
	MainMenuScreen(std::shared_ptr<SDLRenderer> renderer, ScreenManager* screenManager, Client* client);
	~MainMenuScreen();

	/* General Public Methods */
public:
	virtual void update(SDL_Event& events);
	virtual void render();
	virtual void onActivated();
	virtual void onDeactivated();

private:
	std::shared_ptr<SDLRenderer> m_renderer;

	bool m_textInput;
	std::string m_userInput;
	std::string m_userNamePrompt;
	std::string m_serverPrompt;
	unsigned int m_enterCount;

	ScreenManager* m_screenManager;

	Client* m_client;
};

