#pragma once

#include <string>

class ScreenManager
{
public:
	/* Virtual Constructor */
	virtual ~ScreenManager(void) {}

public:
	/* General Public Methods */
	virtual void update() = 0;
	virtual void render() = 0;

public:
	/* Screen Management */
	virtual void setCurrentScreen(std::string window) = 0;

public:
	/* Window Management */
	virtual void createWindow() = 0;
	virtual void setWindowTitle(std::string title) = 0;
	virtual void setWindowSize(int width, int height) = 0;
	virtual void setWindowWidth(int width) = 0;
	virtual void setWindowHeight(int height) = 0;
	virtual void minimiseWindow() = 0;
	virtual void restoreWindow() = 0;
	virtual void maximiseWindow() = 0;
	virtual void setFullScreenMode() = 0;
	virtual void setWindowedMode() = 0;

};