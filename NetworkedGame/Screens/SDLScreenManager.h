#pragma once

#include <iostream>
#include <unordered_map>
#include <vector>
#include <memory>

#ifdef _WIN32
#include <SDL.h>
#elif __APPLE__
    #include <TargetConditionals.h>
    #if TARGET_IPHONE_SIMULATOR
        #include "SDL.h"
    #elif TARGET_OS_MAC
        #include <SDL2/SDL.h>
    #endif
#endif

#include "ScreenManager.h"
#include "Screen.h"
#include "MainMenuScreen.h"
#include "GameScreen.h"
#include "../Renderer/SDLRenderer.h"
#include "../Client.h"


class SDLScreenManager : public ScreenManager
{
	/* Class Constructors & Destructors */
public:
	SDLScreenManager(std::string title, unsigned int width, unsigned int height, Client* client);
	~SDLScreenManager();

	/* General Public Methods */
public:
	void update();
	void render();

	/* Screen Management */
public:
	void setCurrentScreen(std::string window);

private:
	std::unordered_map<std::string, std::shared_ptr<Screen> > m_screenMap;

	/* Window Management */
public:
	void createWindow();
	void setWindowTitle(std::string title);
	void setWindowSize(int width, int height);
	void setWindowWidth(int width);
	void setWindowHeight(int height);
	void minimiseWindow();
	void restoreWindow();
	void maximiseWindow();
	void setFullScreenMode();
	void setWindowedMode();

private:
	SDL_Window* m_window;
	std::shared_ptr<SDLRenderer> m_renderer;
	SDL_Event m_events;
	bool m_running;
	
	unsigned int m_displayDevice;

	std::string m_windowTitle;
	unsigned int m_windowWidth;
	unsigned int m_windowHeight;
	
	SDL_DisplayMode m_displayMode;
	void setUpDisplayModeVector();
	std::vector<SDL_DisplayMode> m_displayModeVector;
};

