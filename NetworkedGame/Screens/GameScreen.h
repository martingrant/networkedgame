#pragma once

#include <string>
#include <vector>

#ifdef _WIN32
#include <SDL.h>
#elif __APPLE__
#include <TargetConditionals.h>
#if TARGET_IPHONE_SIMULATOR
#include "SDL.h"
#elif TARGET_OS_MAC
#include <SDL2/SDL.h>
#endif
#endif

#include "Screen.h"
#include "ScreenManager.h"
#include "../Input/SDLInput.h"
#include "../Renderer/SDLRenderer.h"
#include "../Player.h"
#include "../Client.h"

class GameScreen : public Screen
{
	/* Constructors & Destructors */
public:
	GameScreen(std::shared_ptr<SDLRenderer> renderer, ScreenManager* screenManager, Client* client);
	~GameScreen();

	/* General Public Methods */
public:
	virtual void update(SDL_Event& events);
	virtual void render();
	virtual void onActivated();
	virtual void onDeactivated();

private:
	std::shared_ptr<SDLRenderer> m_renderer;
	ScreenManager* m_screenManager;

	Player player1;
	Player player2;

	bool m_textInput;
	std::string m_userInput;
	std::string m_userPrompt;

	Client* m_client;

	std::vector<std::string> messageLog;
	int messageLogY;
};

