#pragma once

#include <SDL.h>

class Screen
{
public:
	/* Class Constructor & Destructor */
	virtual ~Screen() {}

public:
	/* General Public Methods */
	virtual void update(SDL_Event& events) = 0;
	virtual void render() = 0;
	virtual void onActivated() = 0;
	virtual void onDeactivated() = 0;

};

