#pragma once

#include <iostream>

#ifdef _WIN32
#include <SDL.h>
#elif __APPLE__
    #include <TargetConditionals.h>
    #if TARGET_IPHONE_SIMULATOR
        #include "SDL.h"
    #elif TARGET_OS_MAC
        #include <SDL2/SDL.h>
    #endif
#endif


class SDLController
{
public:
	/* Class Constructor & Destructor */
	SDLController();
	~SDLController(void);

public:
	/* Manage Controller Device */
	void setControllerID(int controllerID);
	bool openController();
	bool closeController();
	bool getControllerStatus();
	int getControllerID() { return m_controllerID; }

	/* Controller data methods */
	float getControllerAxis(const char* axis);
	float getControllerAxis(SDL_GameControllerAxis axis);
	bool getControllerButtonState(const char* button);

private:
	int m_controllerID;
	SDL_GameController* m_controller;
	SDL_Haptic* m_haptic;

	bool m_controllerStatus;

	bool openHaptic();
	bool closeHaptic();
};

