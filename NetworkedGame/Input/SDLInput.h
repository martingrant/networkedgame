#pragma once

#include <utility>
#include <iostream>
#include <unordered_map>

#ifdef _WIN32
#include <SDL.h>
#elif __APPLE__
    #include <TargetConditionals.h>
    #if TARGET_IPHONE_SIMULATOR
        #include "SDL.h"
    #elif TARGET_OS_MAC
        #include <SDL2/SDL.h>
    #endif
#endif

#include "SDLController.h"

class SDLInput
{
public:
	/* Class Constructor & Destructor */
	SDLInput(void);
	~SDLInput(void);

public:
	/* General Public Methods */
	void update();

public:
	/* Keyboard Methods */
	bool getKeyState(const char* key);

public:
	/* Mouse Methods */
	std::pair<int, int> getMousePosition();
	std::pair<int, int> getMouseRelativePosition();
	bool mouseButtonState(unsigned int buttonID);
	int getMouseWheel();

public:
	/* Controller Methods */
	float getControllerAxis(unsigned int controllerID, const char* axis);
	float getControllerAxis(unsigned int controllerID, SDL_GameControllerAxis axis);
	bool getControllerButtonState(unsigned int controllerID, const char* button);
	bool onControllerButtonUp(unsigned int controllerID, const char* button);
	bool onControllerButtonDown(unsigned int controllerID, const char* button);

private:
	/* Controller Methods */
	int detectControllers();
	void detectHaptics();
	bool openController(unsigned int controllerID);
	bool closeController(unsigned int controllerID);
	void pollControllers();

private:
	SDL_Event m_SDLEvent;
	int m_connectedControllers;
	std::unordered_map<int, SDLController> m_controllerList;
};

