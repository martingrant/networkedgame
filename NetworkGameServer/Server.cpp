#include "Server.h"

Server::Server(unsigned int port, unsigned int bufferSize, unsigned int maxSockets)
{
	m_serverStatus = true;

	m_port = port;
	m_bufferSize = bufferSize;
	m_maxSockets = maxSockets;
	m_maxClients = maxSockets - 1;	 // Always (maxSockets - 1), socket for server is required
	m_timeOut = 5000;
	m_connectedClients = 0;

	m_clientList = new Client[m_maxClients];
	m_availableSockets = new bool[m_maxClients];
	m_buffer = new char[m_bufferSize];

	m_messageCodes[0] = "Connection accepted. (Code 0)";
	m_messageCodes[1] = "Connection denied. Server full. (Code 1)";

	m_userCommands[0] = "shutdown";
    
    if (SDLNet_Init() == -1)
    {
        std::cout << "SDLNet_Init: " << SDLNet_GetError() << std::endl;
    }
    
    // Allocate socket set
    if (!(m_socketSet = SDLNet_AllocSocketSet(m_maxSockets)))
    {
        std::cout << "SDLNet_AllocSocketSet: " << SDLNet_GetError() << "\n";
    }
    else
    {
        std::cout << "Allocated socket set:  " << m_maxSockets << " sockets, " << m_maxClients << " client sockets." << std::endl << std::endl;
    }
    
    // Initialise client sockets
    for (size_t index = 0; index < m_maxClients; index++)
    {
        m_clientList[index].m_socket = NULL;
        m_availableSockets[index] = true;
    }
    
    // Resolve server host. Server details now held in m_serverIP, server will listen on m_port
    if (SDLNet_ResolveHost(&m_serverIP, NULL, m_port) < 0)
    {
        std::cout << "SDLNet_ResolveHost: " << SDLNet_GetError() << std::endl;
    }
    else
    {
        std::cout << "Resolved server host to: " << convertIP(m_serverIP) << std::endl << std::endl;
    }
    
    // Open server socket
    if (!(m_serverSocket = SDLNet_TCP_Open(&m_serverIP)))
    {
        std::cout << "SDLNet_TCP_Open: " << SDLNet_GetError() << "\n";
        exit(EXIT_FAILURE);
    }
    else
    {
        std::cout << "Server socket opened." << std::endl << std::endl;
    }
    
    // Add the server socket to the socket set
    SDLNet_TCP_AddSocket(m_socketSet, m_serverSocket);
    
    std::cout << "Waiting for incoming connections." << std::endl << std::endl;
}

Server::~Server(void)
{
	// Close open client sockets
	for (unsigned int index = 0; index < m_maxClients; index++)
	{
		if (m_availableSockets[index] == false)
		{
			SDLNet_TCP_Close(m_clientList[index].m_socket);
			m_availableSockets[index] = true;
		}
	}

	SDLNet_FreeSocketSet(m_socketSet);
	SDLNet_TCP_Close(m_serverSocket);
	SDLNet_Quit();

	delete m_clientList;
	delete m_availableSockets;
	delete m_buffer;
}


bool Server::update()
{
	do
    {
		acceptConnections();
		handleActivity();
	}
    while (m_serverStatus);

	return m_serverStatus;
}


void Server::acceptConnections()
{
	// Check for activity on server socket (incoming connections)
	if (m_socketSet != NULL) 
	{
		SDLNet_CheckSockets(m_socketSet, m_timeOut);
	}

	// Check if server has received any data (to use SDLNet_SocketReady() a socket in a set must have had SDLNet_CheckSockets() called it on before)
	if (SDLNet_SocketReady(m_serverSocket) != 0)
	{
		if (m_connectedClients < m_maxClients)
		{
			// Find available socket 
			int availableSocket = -99;
			for (size_t index = 0; index < m_maxClients; index++)
			{
				if (m_availableSockets[index] == true)
				{
					m_availableSockets[index] = false; // Socket is now taken
					availableSocket = index;            // Store the index
					break;                      
				}
			}

			// Accept the client connection and add socket to the set
			m_clientList[availableSocket].m_socket = SDLNet_TCP_Accept(m_serverSocket);
			SDLNet_TCP_AddSocket(m_socketSet, m_clientList[availableSocket].m_socket);

			m_connectedClients++;

			// Send confirmation to client
			strcpy(m_buffer, m_messageCodes[0]);
			//strcpy_s(m_buffer, strlen(m_buffer), m_messageCodes[0]); // causing buffer size issues
			int msgLength = strlen(m_buffer) + 1;
			SDLNet_TCP_Send(m_clientList[availableSocket].m_socket, (void *)m_buffer, msgLength);

			//// Get username from client
			SDLNet_TCP_Recv(m_clientList[availableSocket].m_socket, m_buffer, m_bufferSize);
			m_clientList[availableSocket].m_userName = m_buffer;
            
            //std::cout << m_clientList[availableSocket].m_userName;

			std::cout << "New client connection accepted: " << " (client #" << availableSocket << ") (" << convertIP(m_clientList[availableSocket].m_socket) << ")" << std::endl;
			std::cout << "(" << m_connectedClients << "/" << m_maxClients << ") client(s) currently connected." << std::endl << std::endl;
		}
		else // Server is full:
		{
			std::cout << "New connection rejected. Server is currently full." << std::endl;

			// Accept the connection to handle situation
			TCPsocket tempSock = SDLNet_TCP_Accept(m_serverSocket);

			// Send client the server is full error code
			strcpy(m_buffer, m_messageCodes[1]);
			//strcpy_s(m_buffer, strlen(m_buffer), m_messageCodes[1]); // causing buffer size issues
			int msgLength = strlen(m_buffer) + 1;
			SDLNet_TCP_Send(tempSock, (void *)m_buffer, msgLength);

			// Close the connection
			SDLNet_TCP_Close(tempSock);
		}
	}
}


void Server::handleActivity()
{
	// Loop to check all connected clients for activity
	for (size_t clientNumber = 0; clientNumber < m_maxClients; clientNumber++)
	{
		// Check if index socket has activity
		if (SDLNet_SocketReady(m_clientList[clientNumber].m_socket) != 0)
		{
			// Check for incoming data, if no data received then client has disconnected
			if (SDLNet_TCP_Recv(m_clientList[clientNumber].m_socket, m_buffer, m_bufferSize) <= 0)
			{
				std::cout << "Client disconneted: " << m_clientList[clientNumber].m_userName << " (client #" << clientNumber << ") (" << convertIP(m_clientList[clientNumber].m_socket) << ")" << std::endl;

				// Remove socket from set, close and reset socket for new connection
				SDLNet_TCP_DelSocket(m_socketSet, m_clientList[clientNumber].m_socket);
				SDLNet_TCP_Close(m_clientList[clientNumber].m_socket);
				m_clientList[clientNumber].m_socket = NULL;
				m_availableSockets[clientNumber] = true;

				m_connectedClients--;

				std::cout << "(" << m_connectedClients << "/" << m_maxClients << ") client(s) currently connected." << std::endl << std::endl;
			}
			else // If client is sending data
			{
				// Print message buffer
				std::cout << m_clientList[clientNumber].m_userName << " (client #" << clientNumber << "):" << m_buffer << std::endl << std::endl;

				// Store which client sent the message
				int originClient = clientNumber;

				// Add username of origin client to beginning of message buffer
				std::stringstream ss;
				ss << ">" << m_clientList[clientNumber].m_userName << " (client #" << originClient << "): " << m_buffer;
				std::string sendString = ss.str();

				// Send message to other clients except client who sent it
				for (size_t index = 0; index < m_maxClients; index++)
				{
					int msgLength = strlen(m_buffer) + 1;

					if (msgLength > 1 && index != originClient && m_availableSockets[index] == false)
					{
						std::cout << "Broadcasting message " << "(" << msgLength << " bytes) from: " << m_clientList[originClient].m_userName << " (client #" << originClient << ") to: " << m_clientList[index].m_userName << " (client #" << index << ")" << std::endl << std::endl;
						SDLNet_TCP_Send(m_clientList[index].m_socket, (void *)m_buffer, msgLength);
					}
				}
				// DEBUG only - Shutdown the server if user requested
				if (strcmp(m_buffer, m_userCommands[0]) == 0)
				{
					m_serverStatus = false;

					std::cout << "Disconnecting all clients and shutting down." << std::endl << std::endl;
				}
			}
		} 
	}
}


std::string Server::convertIP(IPaddress address)
{
	std::stringstream ss;
	Uint8 * dotQuad;

	dotQuad = (Uint8*)&address.host;

	ss << (unsigned short)dotQuad[0] << "." << (unsigned short)dotQuad[1] << '.' << (unsigned short)dotQuad[2] << '.' << (unsigned short)dotQuad[3] << ':' << SDLNet_Read16(&address.port);

	return ss.str();
}


std::string Server::convertIP(TCPsocket socket)
{
	std::stringstream ss;
	Uint8 * dotQuad;

	IPaddress tempIP;

	tempIP.operator=(*SDLNet_TCP_GetPeerAddress(socket));

	// Get IP in dot-quad format, break up 32-bit unsigned host address, split it into array of four 8-but unsigned numbers
	dotQuad = (Uint8*)&tempIP.host;

	// Cast to ints, read last 16 bits for port number
	ss << (unsigned short)dotQuad[0] << "." << (unsigned short)dotQuad[1] << '.' << (unsigned short)dotQuad[2] << '.' << (unsigned short)dotQuad[3] << ':' << SDLNet_Read16(&tempIP.port);

	return ss.str();
}