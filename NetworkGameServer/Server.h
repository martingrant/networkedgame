#pragma once

#include <iostream>
#include <sstream>

#ifdef _WIN32
#include <SDL_net.h>
#elif __APPLE__
#include <SDL2_net/SDL_net.h>
#endif

struct Client {
	TCPsocket m_socket;
	std::string m_userName;
	int m_clientNumber;
};

class Server
{
public:
	Server(unsigned int port, unsigned int bufferSize, unsigned int maxSockets);
	~Server(void);

	bool update();
	void acceptConnections();
	void handleActivity();	

	std::string convertIP(IPaddress address);
	std::string convertIP(TCPsocket socket);

private:
	bool m_serverStatus;	     

	unsigned int m_port;         
	unsigned int m_bufferSize;    
	unsigned int m_maxSockets;    
	unsigned int m_maxClients; 
	unsigned int m_timeOut;
	unsigned int m_connectedClients;

	IPaddress m_serverIP;
	TCPsocket m_serverSocket;
	SDLNet_SocketSet m_socketSet;

	Client* m_clientList;
	bool* m_availableSockets;
	char* m_buffer;

	const char* m_messageCodes[10];
	const char* m_userCommands[10];
};

